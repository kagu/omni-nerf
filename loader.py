import numpy as np
from torch.utils.data import DataLoader, Dataset
import os
import json
import imageio
from PIL import Image
import torch

from camera_utils import make_rand_axis, R_axis_angle

def load_blender_img(basedir,file_path):
    # for blender data without suffix
    fname = os.path.join(basedir, file_path)
    if fname.endswith('png') or fname.endswith('jpg'):
        pass
    else:
        fname = fname + '.png'

    return imageio.imread(fname)


def load_blender_data(basedir, half_res=False, args=None, testskip=1):
    # load blender data, the data is saved in the same format as the nerf synthetic
    # for the real dataset with no ground truth testset, we load the test poses from the json file

    splits = ['train', 'val', 'test']
    metas = {}
    for s in splits:
        with open(
                os.path.join(basedir, 'transforms_{}.json'.format(s)), 'r'
        ) as fp:
            metas[s] = json.load(fp)

    all_imgs = []
    all_poses = []
    counts = [0]
    all_masks = []

    H0, W0 = None, None

    for s in splits:
        meta = metas[s]
        imgs = []
        poses = []
        if s == 'train' or s == 'val' or testskip == 0:
            skip = 1
        else:
            skip = testskip

        for frame in meta['frames'][::skip]:
            if 'file_path' in frame:
                fname = os.path.join(basedir, frame['file_path'])
                # resize images
                im = load_blender_img(basedir,fname)
                if H0 is None:
                    H0,W0 = im.shape[:2]
                    H = int(H0//args.factor)
                    W = int(W0//args.factor)

                im = np.array(Image.fromarray(im).resize((H,W)))
                imgs.append(im)

                if 'mask_path' in frame:
                    # TODO make the mask as alpha channel of the image
                    fname = frame['mask_path']
                    m = load_blender_img(basedir, fname).astype('bool')
                    m = np.array(Image.fromarray(m).resize((H,W))).astype('bool')
                    # resize masks
                    all_masks.append(m)

            else:
                # skip loading the test dataset when no gt_img available

                pass
            poses.append(np.array(frame['transform_matrix']))

        # imgs = (np.array(imgs)[...,:3] / 255.).astype(np.float32)  # keep 3 channels (RGB)
        poses = np.array(poses).astype(np.float32)
        counts.append(counts[-1] + poses.shape[0])

        if imgs:
            imgs = (np.array(imgs)[..., :3] / 255.).astype(np.float32)
            all_imgs.append(imgs)
        all_poses.append(poses)

    i_split = [np.arange(counts[i], counts[i + 1]) for i in range(3)]
    i_train, _, _ = i_split

    imgs = np.concatenate(all_imgs, 0)
    poses = np.concatenate(all_poses, 0)
    if all_masks:
        masks = np.array(all_masks)
    else:
        masks = None

    # TODO test loading of the focal len
    focal_raw = meta['fl_x']
    # scale the focal length
    focal = focal_raw * W / W0

    noisy_focal = focal

    if args.initial_noise_size_intrinsic != 0.0:
        noisy_focal = noisy_focal * (1 + args.initial_noise_size_intrinsic)
        print("Starting with noise in intrinsic parameters")

    poses_update = poses.copy()

    if args.initial_noise_size_rotation != 0.0:
        angle_noise = (
                              np.random.rand(poses.shape[0], 1) - 0.5
                      ) * 2 * args.initial_noise_size_rotation * np.pi / 180
        rotation_axis = make_rand_axis(poses.shape[0])
        rotmat = R_axis_angle(rotation_axis, angle_noise)

        poses_update[i_train, :3, :3] = rotmat[i_train] @ \
                                        poses_update[i_train, :3, :3]

        print("Starting with noise in rotation matrices")

    if args.initial_noise_size_translation != 0.0:
        individual_noise = (
                                   np.random.rand(poses.shape[0], 3) - 0.5
                           ) * 2 * args.initial_noise_size_translation

        assert (
            np.all(
                np.abs(individual_noise) < args.initial_noise_size_translation
            )
        )

        poses_update[i_train, :3, 3] = poses_update[i_train, :3, 3] + \
                                       individual_noise[i_train]
        print("Starting with noise in translation parameters")

    raw_intrinsic = torch.tensor([
        [focal, 0, W / 2, 0],
        [0, focal, H / 2, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1]
    ]).float()

    print(f"Original focal length : {focal}\n")
    print(f"Initial noisy focal length : {noisy_focal}\n")

    # use test poses as render poses
    render_poses = torch.from_numpy(poses[i_split[2]]).float()

    raw_extrinsic = torch.from_numpy(poses).float()

    return imgs, poses_update, render_poses, [H, W, noisy_focal], i_split, \
           (raw_intrinsic, raw_extrinsic), masks