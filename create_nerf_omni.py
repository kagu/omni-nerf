import os
import numpy as np
import torch
import torch.optim as optim
import math
from torch import Tensor
from typing import List
import torch.nn as nn
import sys

from run_nerf_helpers import (
    get_embedder,
    NeRF,
)

from camera_model import CameraModel
from image_correction_model import ExposureModel


def run_network(inputs, viewdirs, fn, embed_fn, embeddirs_fn, netchunk=1024 * 64):
    """Prepares inputs and applies network 'fn'.
    """
    inputs_flat = torch.reshape(inputs, [-1, inputs.shape[-1]])
    embedded = embed_fn(inputs_flat)

    if viewdirs is not None:
        input_dirs = viewdirs[:, None].expand(inputs.shape)
        input_dirs_flat = torch.reshape(input_dirs, [-1, input_dirs.shape[-1]])
        embedded_dirs = embeddirs_fn(input_dirs_flat)
        embedded = torch.cat([embedded, embedded_dirs], -1)

    outputs_flat = batchify(fn, netchunk)(embedded)
    outputs = torch.reshape(outputs_flat, list(inputs.shape[:-1]) + [outputs_flat.shape[-1]])
    return outputs

def create_nerf(
    args, noisy_focal, noisy_poses, H, W, mode="train", device="cuda"
):
    """Instantiate NeRF's MLP model.
    """

    camera_model = None

    embed_fn, input_ch = get_embedder(args.multires, args.i_embed)

    input_ch_views = 0
    embeddirs_fn = None
    if args.use_viewdirs:
        embeddirs_fn, input_ch_views = get_embedder(
            args.multires_views, 
            args.i_embed
        )
    output_ch = 5 if args.N_importance > 0 else 4
    skips = [4]
    model = NeRF(D=args.netdepth, W=args.netwidth, input_ch=input_ch, 
                 output_ch=output_ch, skips=skips, 
                 input_ch_views=input_ch_views, use_viewdirs=args.use_viewdirs)
    model = nn.DataParallel(model).to(device)
    grad_vars = list(model.parameters())
    
    model_fine = None
    if args.N_importance > 0:
        model_fine = NeRF(D=args.netdepth_fine, W=args.netwidth_fine,
            input_ch=input_ch, output_ch=output_ch, skips=skips,
            input_ch_views=input_ch_views, use_viewdirs=args.use_viewdirs)
        model_fine = nn.DataParallel(model_fine).to(device)
        grad_vars += list(model_fine.parameters())

    network_query_fn = lambda inputs, viewdirs, network_fn: run_network(
        inputs, viewdirs, network_fn, embed_fn=embed_fn, 
        embeddirs_fn=embeddirs_fn, netchunk=args.netchunk_per_gpu * args.n_gpus)
    
    render_kwargs_train = {
        'network_query_fn': network_query_fn,
        'perturb': args.perturb,
        'N_importance': args.N_importance,
        'network_fine': model_fine,
        'N_samples': args.N_samples,
        'network_fn': model,
        'use_viewdirs': args.use_viewdirs,
        'white_bkgd': args.white_bkgd,
        'raw_noise_std': args.raw_noise_std,
    }

    # do not use ndc by default
    print('Not ndc!')
    render_kwargs_train['ndc'] = False
    render_kwargs_train['lindisp'] = args.lindisp

    render_kwargs_test = {
        k: render_kwargs_train[k] for k in render_kwargs_train
    }
    render_kwargs_test['perturb'] = False
    render_kwargs_test['raw_noise_std'] = 0.



    # create camera model and image correction model
    if args.use_GEAR360:
        # use default camera relative position of samsung gear 360
        ref = []
        for i in range(noisy_poses.shape[0]):
            if i%2 == 0:
                ref.append(i)
            else:
                ref.append(i-1)

        rel_r_init = [0.0, np.pi, 0.0]
        rel_t_init = [0, 0, -0.045]

        camera_model = CameraModel(H,W, noisy_poses.shape[0],noisy_focal,noisy_poses,
                                   args, coeff_init=args.coeff_init,
                                   use_relative_pose = True,
                                   ref = ref,
                                   rel_t_init = rel_t_init,
                                   rel_r_init = rel_r_init,)
    else:
        camera_model = CameraModel(H,W, noisy_poses.shape[0],noisy_focal,noisy_poses, args, coeff_init=args.coeff_init )

    camera_model = camera_model.to(device)

    exposure_model = ExposureModel(noisy_poses.shape[0], H,W, args.learn_expo, args.learn_expo)
    exposure_model = exposure_model.to(device)

    # Create optimizers for different parameters
    nerf_optimizer = torch.optim.Adam(params=grad_vars, lr=args.nerf_lr)

    camera_optimizer = torch.optim.Adam(params = camera_model.parameters(),
                                        lr = args.camera_lr)

    exposure_optimizer = torch.optim.Adam(params = exposure_model.parameters(),
                                          lr = args.exposure_lr)

    optimizer_dict = {
        'nerf':nerf_optimizer,
        'camera':camera_optimizer,
        'exposure':exposure_optimizer
    }

    scheduler_dict = {}
    # Create scheduler
    for k in optimizer_dict.keys():
        lr_decay = getattr(args, k+'_lr_decay')
        scheduler = torch.optim.lr_scheduler.ExponentialLR(
            optimizer_dict[k],
            gamma=0.1**(1/lr_decay/1000)
        )
        scheduler_dict[k] = scheduler

    ##########################

    start = 0
    basedir = args.basedir
    expname = args.expname

    # Load checkpoints
    if args.ft_path is not None and args.ft_path != 'None':
        ckpts = [args.ft_path]
    else:
        ckpts = [
            os.path.join(basedir, expname, f) for f in sorted(
                os.listdir(os.path.join(basedir, expname))
            ) if 'tar' in f
        ]

    print('Found ckpts', ckpts)
    
    if len(ckpts) > 0 and not args.no_reload:
        ckpt_path = ckpts[-1]
        print('Reloading from', ckpt_path)
        ckpt = torch.load(ckpt_path, map_location=device)

        # Load model
        # drop the learned radiance field for optimized poses
        if not args.load_camera_only:

            start = ckpt['global_step']

            model.load_state_dict(ckpt['network_fn_state_dict'])
            if not model_fine is None:
                model_fine.load_state_dict(ckpt['network_fine_state_dict'])

            if not exposure_model is None and "exposure_model" in ckpt.keys():
                exposure_model.load_state_dict(ckpt["exposure_model"])

            for k in optimizer_dict:
                if k+'optimizer_state_dict' in ckpt.keys():
                    optimizer_dict[k].load_state_dict(ckpt[k+'optimizer_state_dict'])
                if k+'scheduler_state_dict' in ckpt.keys():
                    scheduler_dict[k].load_state_dict(ckpt[k+'scheduler_state_dict'])

        if not camera_model is None and "camera_model" in ckpt.keys():
            print('camera model loaded')
            camera_model.load_state_dict(ckpt["camera_model"])

    ##########################

    return (
        render_kwargs_train, 
        render_kwargs_test, 
        start, 
        grad_vars, 
        optimizer_dict,
        scheduler_dict,
        camera_model,
        exposure_model
    )


def batchify(fn, chunk):
    """Constructs a version of 'fn' that applies to smaller batches.
    """
    if chunk is None:
        return fn

    def ret(inputs):
        return torch.cat([fn(inputs[i:i + chunk]) for i in range(0, inputs.shape[0], chunk)], 0)

    return ret