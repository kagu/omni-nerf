import os

import numpy as np
import imageio

def evaluate_camera():

    pass

def evaluate_exposure(hwf, mask,exposure_model, i_train,
                    savedir=None):
    H,W,_ = hwf
    H = int(H)
    W = int(W)

    val_ind = i_train[0]
    u, v = np.meshgrid(np.linspace(0, W - 1, W),
                       np.linspace(0, H - 1, H))
    print(mask.shape, 'mask shape')
    if len(mask.shape) >2:
        mask = mask[val_ind]
    else:
        mask = mask
    u = u[mask]
    v = v[mask]
    scale,shift = exposure_model([val_ind]*len(u),u,v)



    temp = np.zeros((H,W),)
    factor = 0.5*(scale.cpu().numpy()+0.5)+shift.cpu().numpy()
    # rescale to make it clear
    factor = (factor - 0.4)/0.2

    temp[mask] = factor

    if savedir is not None:
        filename = os.path.join(savedir,'exposure_mask_0.png')
        imageio.imwrite(filename,temp)
