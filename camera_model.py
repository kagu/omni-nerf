import numpy as np
import torch
import torch.nn as nn
from pytorch3d_utils import axis_angle_to_matrix


def make_c2ws(r,t):
    temp_ext = torch.zeros((r.shape[0],4,4), device=r.device)
    # (...,3) euler angles to (...,3,3)  rotation matrix
    R_s = axis_angle_to_matrix(r)

    temp_ext[:,:3,:3] = R_s
    temp_ext[:,:3,3] = t
    temp_ext[:,3,3] = 1
    return temp_ext


class CameraModel(nn.Module):

    def __init__(
            self, H, W, num_cams,
            focal_init,
            init_c2w,
            args,
            coeff_init = (0,0,0),
            learn_intrinsic = True,
            learn_extrinsic = True,

            use_relative_pose=False,
            learn_relative_pose = False,
            ref = None,
            rel_t_init = (0,0,0),
            rel_r_init = (0,0,0),
            ):

        # pixel coordinate u,v to camera local coordinate x0,y0

        super(CameraModel, self).__init__()
        self.H = H
        self.W = W
        self.args = args
        self.num_cams = num_cams
        self.focal = nn.Parameter(torch.tensor(focal_init, dtype=torch.float32),requires_grad=learn_intrinsic)

        self.intrinsics = nn.ParameterDict(
        {
            'alpha':nn.Parameter(torch.tensor(1.0, dtype=torch.float32), requires_grad=learn_intrinsic),
            'beta': nn.Parameter(torch.tensor(1.0, dtype=torch.float32), requires_grad=learn_intrinsic),
            's': nn.Parameter(torch.tensor(0.0, dtype=torch.float32), requires_grad=learn_intrinsic),
            'x0': nn.Parameter(torch.tensor(0.0, dtype=torch.float32), requires_grad=learn_intrinsic),
            'y0': nn.Parameter(torch.tensor(0.0, dtype=torch.float32), requires_grad=learn_intrinsic)
        }
        )

        # initialize the polynomial model
        self.learn_projection = learn_intrinsic
        self.coefficients = nn.Parameter(torch.tensor(coeff_init, dtype=torch.float32), requires_grad=learn_intrinsic)

        # initialize the relative transform
        if not use_relative_pose:
            ref = np.arange(num_cams)
            learn_relative_pose = False

        self.use_relative_pose = use_relative_pose
        self.ref = np.array(ref, dtype='int')
        self.rel_t = nn.Parameter(torch.tensor([(0,0,0),rel_t_init],dtype=torch.float32), requires_grad = learn_relative_pose)
        self.rel_r = nn.Parameter(torch.tensor([(0,0,0),rel_r_init],dtype=torch.float32),requires_grad = learn_relative_pose)

        # initialize camera poses
        if init_c2w is not None:
            self.init_c2w = nn.Parameter(torch.tensor(init_c2w,dtype=torch.float32), requires_grad=False)
        else:
            self.init_c2w = None

        # parameterize the rotation by the angle-axis presentation of so(3)
        self.r = nn.Parameter(torch.zeros(size=(int(num_cams), 3), dtype=torch.float32), requires_grad=learn_extrinsic)  # (N, 3)
        self.t = nn.Parameter(torch.zeros(size=(int(num_cams), 3), dtype=torch.float32), requires_grad=learn_extrinsic)  # (N, 3)

    def get_pose(self, idx):
        if len(idx.shape)>1:
            # if idx is the c2w matrix, return directly the c2w
            return idx.to(self.t.device)

        ref_index = idx

        ref_mask = ~np.any(self.ref[:,np.newaxis] == idx,axis=0).astype('int')

        if self.use_relative_pose:
            ref_index = self.ref[idx]
            # TODO check the shape of rel_t and rel_r
            # add relative translation and rotation to the delta t/r

            r = self.r[ref_index]*self.args.delta_rotation_scale + self.rel_r[ref_mask]
            t = self.t[ref_index]*self.args.delta_translation_scale + self.rel_t[ref_mask]
        else:
            r = self.r[ref_index]*self.args.delta_rotation_scale
            t = self.t[ref_index]*self.args.delta_translation_scale

        # make the transform matrix from r and t vector
        R_delta = make_c2ws(r,t) # [4,4]

        # apply the delta R to c2w
        c2w = R_delta
        if self.init_c2w is not None:
            if c2w.dim() == 3:
                c2w = torch.einsum('ijk,ikl->ijl', self.init_c2w[ref_index], c2w)
            else:
                c2w = self.init_c2w[ref_index] @ c2w
        return c2w

    # get rays from pixel coordinates and camera indices
    def forward(self,u,v,idx, fisheye=True, focal=None):
        if focal is None:
            focal = self.focal

        u = torch.tensor(u, dtype=torch.float32, device=self.r.device)
        v = torch.tensor(v, dtype=torch.float32, device=self.r.device)

        # 0.5 pixel offset necessary?
        u = u - 0.5*self.W + 0.5 + self.intrinsics['x0']
        v = v - 0.5*self.H + 0.5 + self.intrinsics['y0']
        y_c = v/(focal*self.intrinsics['beta'])
        x_c = (u-v*self.intrinsics['s'])/(focal*self.intrinsics['alpha'])
        r_d = torch.sqrt(y_c ** 2 + x_c ** 2)

        if self.learn_projection & fisheye:
            # learn the polynomial coefficients
            theta_d = torch.arctan(r_d)
            a, b, c = self.coefficients
            # use following approximation
            theta_u = theta_d + a * theta_d ** 3 + b * theta_d ** 5 + c * theta_d ** 7
        elif fisheye:
            # use ground truth projection function
            theta_u = self.projection_mapping['equisolid'](r_d)
        else:
            theta_d = torch.arctan(r_d)
            theta_u = theta_d

        # corrected pixel coordinates (rectified)
        # normalized vector d
        r_u = torch.sin(theta_u)
        x_0 = r_u * x_c / r_d
        y_0 = - r_u * y_c / r_d
        z_0 = -torch.cos(theta_u)

        dirs = torch.stack([x_0, y_0, z_0], dim=-1)

        c2ws = self.get_pose(idx)
        if c2ws.dim() == 3:
            rays_d = torch.sum(dirs[..., np.newaxis, :] * c2ws[:, :3, :3], -1)
            rays_o = c2ws[:,:3,-1]
        else:
            rays_d = torch.sum(dirs[..., np.newaxis, :] * c2ws[:3, :3], -1)
            rays_o = c2ws[:3, -1].expand(rays_d.shape)

        return rays_o, rays_d

    def get_extrinsic(self):
        ind = np.arange(int(self.num_cams))
        return self.get_pose(ind)

    def learn_extrinsics(self,flag):
        self.r.requires_grad_(flag)
        self.t.requires_grad_(flag)
        pass

    def learn_intrinsics(self,flag):
        self.focal.requires_grad_(flag)
        for k in self.intrinsics.keys():
            self.intrinsics[k].requires_grad_(flag)

    def learn_distortion(self, flag):
        self.coefficients.requires_grad_(flag)

    def learn_relative_pose(self, flag):
        self.rel_t.requires_grad_(flag)
        self.rel_r.requires_grad_(flag)

