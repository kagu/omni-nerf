import torch
import numpy as np

def get_rays_kps_use_camera(H,
                            W,
                            camera_model,
                            kps_list,
                            idx_in_camera_param=None,
                            extrinsic= None,
                            enable_distortion = True,
                            enable_od = True
                            ):
    """get rays from flatten pixel coordinates (W*H-border_pxiels, 2)"""
    assert kps_list[:, 0].max() < W
    assert kps_list[:, 1].max() < H
    assert (
        idx_in_camera_param is None and not extrinsic is None or \
        not idx_in_camera_param is None and extrinsic is None
        )

    kps_list_expand = torch.stack(
        [kps_list[:, 0]+0.5, kps_list[:, 1]+0.5,
         torch.ones_like(kps_list[:, 0])],
        dim=-1).float()


    intrinsics_inv = torch.inverse(camera_model.get_intrinsic()[:3, :3])

    extrinsic = camera_model.get_extrinsic()[idx_in_camera_param] \
        if extrinsic is None else extrinsic

    dirs = torch.einsum("ij, kj -> ik", kps_list_expand,
                        intrinsics_inv)
    dirs[:, 1:3] = -dirs[:, 1:3]

    if extrinsic.dim() == 3:
        rays_d = torch.sum(dirs[..., np.newaxis, :] * extrinsic[:, :3, :3], -1)
        rays_o = extrinsic[:, :3, -1]
    else:
        rays_d = torch.sum(dirs[..., np.newaxis, :] * extrinsic[:3, :3], -1)
        rays_o = extrinsic[:3, -1].expand(rays_d.shape)

    if hasattr(camera_model, "ray_o_noise") and enable_od:
        kps_list = kps_list.long()
        ray_o_noise = camera_model.get_ray_o_noise().reshape(H, W, 3)
        ray_o_noise_kps = ray_o_noise[kps_list[:, 1], kps_list[:, 0]]
        rays_o = rays_o + ray_o_noise_kps

    if hasattr(camera_model, "ray_d_noise") and enable_od:
        kps_list = kps_list.long()
        ray_d_noise = camera_model.get_ray_d_noise().reshape(H, W, 3)
        ray_d_noise_kps = ray_d_noise[kps_list[:, 1], kps_list[:, 0]]

        rays_d = rays_d + ray_d_noise_kps
        rays_d = rays_d / (rays_d.norm(dim=1)[:, None] + 1e-10)

    return rays_o, rays_d


# def render_ray_from_camera(
#         camera_model, camera_idx, select_inds, rank, extrinsic=None
# ):
#     '''
#     :param H: image height
#     :param W: image width
#     :param intrinsics: 4 by 4 intrinsic matrix
#     :param c2w: 4 by 4 camera to world extrinsic matrix
#     :return:
#     '''
#     W, H = camera_model.W, camera_model.H
#     if not camera_idx is None:
#         intrinsics, c2w = camera_model(camera_idx)
#     else:
#         assert extrinsic is not None
#         intrinsics = camera_model.get_intrinsic()
#         c2w = torch.from_numpy(extrinsic).to(rank)
#
#     N_rand = len(select_inds)
#
#     if isinstance(select_inds, torch.Tensor):
#         select_inds = select_inds.cpu().detach().numpy()
#
#     u, v = np.meshgrid(np.arange(W), np.arange(H))
#     u = u.reshape(-1)[select_inds].astype(dtype=np.float32) + 0.5
#     v = v.reshape(-1)[select_inds].astype(dtype=np.float32) + 0.5
#     pixels = np.stack((u, v, np.ones_like(u)), axis=0)
#     pixels = torch.from_numpy(pixels).to(rank)
#
#     cx, cy = intrinsics[0, 2], intrinsics[1, 2]
#
#     if hasattr(camera_model, "distortion_noise"):
#         (k0, k1) = camera_model.get_distortion()
#         center = torch.stack([cx, cy]).view(2, -1)
#         r2 = (pixels[:2] - center) / center
#         pixels[:2] =  (pixels[:2] - center) * \
#                       (1 + r 2* *2 * k0 + r 2* *4 * k1) + center
#
#
#     intrinsic_inv = torch.zeros_like(intrinsics[:3, :3])
#     intrinsic_inv[[0, 1, 0, 1, 2], [0, 1, 2, 2, 2]] += torch.stack(
#         [
#             1.0 / intrinsics[0][0], 1.0 / intrinsics[1][1],
#             - intrinsics[0][2] / intrinsics[0][0],
#             - intrinsics[1][2] / intrinsics[1][1], torch.tensor(1.0).to(rank)
#         ]
#     )
#
#     rays_d = intrinsic_inv @ pixels
#     rays_d = c2w[:3, :3] @ rays_d
#     rays_d = torch.transpose(rays_d, 1, 0)
#
#     rays_o = c2w[:3, 3].view(1, 3).repeat(N_rand, 1).to(rank)
#
#     if hasattr(camera_model, "ray_o_noise"):
#         rays_o = rays_o + camera_model.get_ray_o_noise()[select_inds].to(rank)
#
#     if hasattr(camera_model, "ray_d_noise"):
#         rays_d = rays_d + camera_model.get_ray_d_noise()[select_inds].to(rank)
#         rays_d = rays_d / rays_d.norm(dim=1, keepdim=True)
#
#     depth = c2w.T[2, 3] * torch.ones((rays_o.shape[0],)).to(rank)
#
#     return rays_o, rays_d, depth