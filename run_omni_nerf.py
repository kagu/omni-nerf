import os
import sys
import imageio
import time
import random
import numpy as np

import torch

from torch.utils.tensorboard import SummaryWriter

from evaluate import evaluate_exposure

from tqdm import tqdm, trange
from datetime import datetime

from run_nerf_helpers import (
    fix_seeds,
    img2mse,
    mse2psnr
)

# Ray generating functions

from get_rays_omni import get_rays_kps_use_camera

# from render import (
#     render,
#     render_path
# )

# Argument Parser
from config_argparse import config_parser

# DataLoader
from loader import load_blender_data

# NeRF network
from create_nerf_omni import create_nerf

# load spatial grid data
from omni_utils import get_fisheye_mask
from render_masked import render, render_path


sys.path.insert(0, "thirdparty/nerfmm/")

from thirdparty.nerfmm.utils.align_traj import align_ate_c2b_use_a2b

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def train():
    parser = config_parser()
    args = parser.parse_args()

    fix_seeds(args.seed)

    # Multi-GPU
    args.n_gpus = torch.cuda.device_count()
    print(f"Using {args.n_gpus} GPU(s).")

    if args.dataset_type == 'blender':
        # we support only this type of data
        (
            images, noisy_extrinsic, render_poses, hwf, i_split, raw_camera_info, mask
        ) = load_blender_data(args.datadir, args.half_res, args, args.testskip)

        print("Loaded blender dataset")
        print("Images shape : {}".format(images.shape))
        print("HWF : {}".format(hwf))
        print("Directory path of data : {}".format(args.datadir))

        (raw_intrinsic, raw_extrinsic) = raw_camera_info
        i_train, i_val, i_test = i_split

        # near and far bound of the sampling
        near = args.near
        far = args.far


        if args.white_bkgd:
            images = images[..., :3] * images[..., -1:] + \
                     (1. - images[..., -1:])
        else:
            images = images[..., :3]

    else:
        assert False, "Invalid Dataset Selected"
        return

    noisy_train_poses = noisy_extrinsic[i_train]

    # Cast intrinsics to right types
    H, W, noisy_focal = hwf
    H, W = int(H), int(W)
    hwf = [H, W, noisy_focal]

    if args.train_fisheye or args.render_fisheye:

        fisheye_mask = get_fisheye_mask(H, W, args.radius)


    # Create log dir and copy the config file
    basedir = args.basedir
    expname = args.expname
    os.makedirs(os.path.join(basedir, expname), exist_ok=True)
    f = os.path.join(basedir, expname, 'args.txt')
    with open(f, 'w') as file:
        for arg in sorted(vars(args)):
            attr = getattr(args, arg)
            file.write('{} = {}\n'.format(arg, attr))
    if args.config is not None:
        f = os.path.join(basedir, expname, 'config.txt')
        with open(f, 'w') as file:
            file.write(open(args.config, 'r').read())

    writer = SummaryWriter(os.path.join(basedir, expname))

    # Create nerf model
    (
        render_kwargs_train, render_kwargs_test, start,
        grad_vars, optimizer_dict,scheduler_dict, camera_model, exposure_model
    ) = create_nerf(
        args, noisy_focal, noisy_train_poses, H, W, mode="train", device=device
    )

    global_step = start

    bds_dict = {
        'near': near,
        'far': far,
    }
    render_kwargs_train.update(bds_dict)
    render_kwargs_test.update(bds_dict)

    # Move testing data to GPU
    render_poses = render_poses.to(device)

    # Prepare raybatch tensor if batching random rays
    N_rand = args.N_rand

    u, v = np.meshgrid(np.linspace(0, W - 1, W),
                       np.linspace(0, H - 1, H))

    if args.render_only:
        print('RENDER ONLY')
        with torch.no_grad():
            testsavedir = os.path.join(basedir, expname,
                                       'renderonly_{}_{:06d}'.format('test' if args.render_test else 'path', start))
            os.makedirs(testsavedir, exist_ok=True)
            to8b = lambda x: (255 * np.clip(x, 0, 1)).astype(np.uint8)
            print('test poses shape', render_poses.shape)

            render_poses_expand = align_ate_c2b_use_a2b(
                raw_extrinsic[i_train],
                camera_model.get_extrinsic().detach(),
                render_poses
            )
            ## a perspective camera preset
            # h = 720
            # w = 1280
            # test_hwf = (h, w, 30/35*w)
            # _hwf = test_hwf

            _hwf = (hwf[0], hwf[1], None)

            rgbs, *_ = render_path(
                render_poses_expand,
                _hwf, args.chunk, render_kwargs_test,
                savedir=testsavedir, mode="test",
                camera_model=camera_model, args=args,
                transform_align=render_poses_expand,
                mask=fisheye_mask if args.render_fisheye else None,
            )

            print('Done rendering', testsavedir)
            imageio.mimwrite(os.path.join(testsavedir, 'video.mp4'), to8b(rgbs), fps=30, quality=8)

            return

    use_batching = not args.no_batching

    if use_batching:
        # flatten the train pixel coordinates
        u = u.flatten()
        v = v.flatten()
        # test this!!!!
        img_indices = []
        pixel_indices = []
        img_indices_precrop = []
        pixel_indices_precrop = []

        mask_center = get_fisheye_mask(H,W, args.precrop_frac*(W/2))

        if not args.train_fisheye:
            for img_ind, im in enumerate(images[i_train]):
                img_indices.append(np.ones(len(u))*img_ind)
                pixel_indices.append(np.arange(len(u)))

                # precrop also for perspective

        else:
            for im_ind, im in enumerate(images[i_train]):
                if mask is not None:
                    if len(mask.shape) > 2:
                        mask_i = mask[im_ind]

                    else:
                        mask_i = mask
                else:
                    mask_i = fisheye_mask


                img_indices.append(np.ones(np.sum(mask_i),dtype=int)*im_ind)
                pixel_indices.append(np.where(mask_i.flatten())[0])

                if args.precrop_iters>0:
                    mask_precrop = mask_i & mask_center
                    img_indices_precrop.append(np.ones(np.sum(mask_precrop), dtype=int) * im_ind)
                    pixel_indices_precrop.append(np.where(mask_precrop.flatten())[0])

            img_indices = np.concatenate(img_indices) # flattened image indices
            pixel_indices = np.concatenate(pixel_indices) # flattened pixel indices (masked)

            if args.precrop_iters>0:
                img_indices_precrop = np.concatenate(img_indices_precrop)
                pixel_indices_precrop = np.concatenate(pixel_indices_precrop)

        # get shuffled pixel indices
        shuffled_ray_idx = np.arange(len(img_indices))
        np.random.shuffle(shuffled_ray_idx)
        if args.precrop_iters>0:
            shuffled_ray_idx_precrop = np.arange(len(img_indices_precrop))
            np.random.shuffle(shuffled_ray_idx_precrop)

        # shuffled_image_idx = img_indices[shuffled_ray_idx]
        i_batch = 0

    # Move training data to GPU
    # images = torch.tensor(images, dtype=torch.float32).to(device)

    noisy_extrinsic = torch.tensor(noisy_extrinsic, dtype=torch.float32).to(device)

    N_iters = 200000 + 1
    N_iters = args.N_iters if args.N_iters is not None else N_iters
    print(f"N-iters: {N_iters}")

    print("TRAIN views are {}".format(i_train))
    print("VAL views are {}".format(i_val))
    print("TEST views are {}".format(i_test))

    start = start + 1
    for i in trange(start, N_iters):

        # activate learnable parameters from iterations
        if i == start and i < args.add_i and camera_model is not None:
            camera_model.learn_intrinsics(False)

        if i == start and i < args.add_e and camera_model is not None:
            camera_model.learn_extrinsics(False)

        if i == start and i < args.add_radial and camera_model is not None:
            camera_model.learn_distortion(False)

        if i == args.add_i and camera_model is not None:
            camera_model.learn_intrinsics(True)
            print("Activated learnable i")

        if i == args.add_e and camera_model is not None:
            camera_model.learn_extrinsics(True)
            print("Activated learnable e")

        if i == args.add_radial and camera_model is not None:
            camera_model.learn_distortion(True)
            print("Activated learnable distortion")

        time0 = time.time()

        # Sample random ray batch

        if use_batching:
            # sample annealing from Reg-Nerf
            sample_annealing = False
            annealing_steps = 10000
            if sample_annealing:
                near = args.near
                far = args.far
                mid = (far - near)/2

                anneal_factor = np.min((1,global_step+5000/annealing_steps))

                near = mid - (mid-near) * anneal_factor
                far = mid + (far-mid) * anneal_factor
                render_kwargs_train.update(
                    {'near':near,
                     'far':far}
                )


            if i < args.precrop_iters:
                img_indices_iter = img_indices_precrop
                shuffled_ray_idx_iter = shuffled_ray_idx_precrop
                pixel_indices_iter = pixel_indices_precrop

                if i == start:
                    print(
                        "[Config] Center cropping until iter {}".format(
                            args.precrop_iters
                        )
                    )
            else:
                img_indices_iter = img_indices
                shuffled_ray_idx_iter = shuffled_ray_idx
                pixel_indices_iter = pixel_indices

            image_idx_curr_step = img_indices_iter[shuffled_ray_idx_iter[i_batch:i_batch + N_rand]]

            pixel_index = pixel_indices_iter[shuffled_ray_idx_iter[i_batch:i_batch + N_rand]]
            h_list = v[pixel_index]
            w_list = u[pixel_index]

            select_coords = np.stack([w_list, h_list], -1)
            assert select_coords[:, 0].max() < W
            assert select_coords[:, 1].max() < H

            # image_idx_curr_step_tensor = torch.from_numpy(
            #     image_idx_curr_step
            # ).long().cuda()
            #
            # kps_list = torch.from_numpy(select_coords).cuda()

            rays_o, rays_d = camera_model(w_list, h_list, image_idx_curr_step, )

            scale, shift = exposure_model(image_idx_curr_step, w_list, h_list, )

            batch_rays = torch.stack([rays_o, rays_d])
            index_train = i_train[
                image_idx_curr_step
            ]
            target_s = images.reshape(images.shape[0],-1, images.shape[-1])[index_train,pixel_index]

            # target_s = images[index_train].reshape(len(index_train),-1,3)[pixel_index]
            # move pixels to gpu
            target_s = torch.tensor(target_s, dtype=torch.float32, device=device)
            img_i = np.random.choice(index_train)
            img_i_train_idx = np.where(i_train == img_i)[0][0]

            i_batch += N_rand
            if i_batch >= len(shuffled_ray_idx_iter):
                print("Shuffle data after an epoch!")
                np.random.shuffle(shuffled_ray_idx_iter)
                i_batch = 0

        else:
            # TODO train with non batch input
            # always use batch for the moment
            pass

        # render pixels
        rgb, disp, acc, extras = render(
            H=H, W=W, chunk=args.chunk, rays=batch_rays,
            verbose=i < 10, retraw=True, camera_model=camera_model,
            mode="train", **render_kwargs_train,
        )

        # auto exposure
        rgb_comp = (rgb - shift.unsqueeze(-1)) / (scale.unsqueeze(-1) + 0.5)
        
        # regularize the auto-exposure terms
        expo_reg = 0.0*(torch.sum(torch.abs(scale-0.5))+torch.sum(torch.abs(shift)))


        train_loss_1 = img2mse(rgb_comp, target_s)
        trans = extras['raw'][..., -1]
        train_psnr_1 = mse2psnr(train_loss_1)
        train_loss = train_loss_1 + expo_reg

        if 'rgb0' in extras:
            rgb0_comp = (extras['rgb0'] - shift.unsqueeze(-1)) / (scale.unsqueeze(-1) + 0.5)
            train_loss_0 = img2mse(rgb0_comp, target_s)
            train_loss = train_loss + train_loss_0
            train_psnr_0 = mse2psnr(train_loss_0)

        # try to cumulate the grad?
        for optimizer in optimizer_dict.values():
            optimizer.zero_grad()
        train_loss.backward()
        for optimizer in optimizer_dict.values():
            optimizer.step()
        for scheduler in scheduler_dict.values():
            scheduler.step()


        if not camera_model is None and global_step % 2000 == 1:
            # TODO
            # log the learned delta transformation

            # log the validated image
            pass


        dt = time.time() - time0

        # Rest is logging
        if i % args.i_weights == 0:
            path = os.path.join(basedir, expname, '{:06d}.tar'.format(i))
            save_dict = {
                'global_step': global_step,
                'network_fn_state_dict': (render_kwargs_train['network_fn'].state_dict()),
                'network_fine_state_dict': (render_kwargs_train['network_fine'].state_dict()),
                'camera_model': camera_model.state_dict(),
                'exposure_model': exposure_model.state_dict(),
            }
            # save optimizer and scheduler
            for k in optimizer_dict.keys():
                save_dict[k+'optimizer_state_dict'] = optimizer_dict[k].state_dict()
                save_dict[k+'scheduler_state_dict'] = scheduler_dict[k].state_dict()

            torch.save(save_dict, path)
            print('Saved checkpoints at', path)

        test_render = (i % args.i_testset == 0 and i > 0)
        val_render = (i % args.i_img == 0)

        if val_render:
            gt_transformed_pose_val = align_ate_c2b_use_a2b(
                raw_extrinsic[i_train],
                camera_model.get_extrinsic().detach(),
                raw_extrinsic[i_val]
            )

        if test_render:
            gt_transformed_pose_test = align_ate_c2b_use_a2b(
                raw_extrinsic[i_train],
                camera_model.get_extrinsic().detach(),
                raw_extrinsic[i_test]
            )

        # Validate Rendering
        if val_render:
            # if False:

            print("Starts validate Rendering")
            with torch.no_grad():
                testsavedir = os.path.join(
                    basedir, expname, 'valset_{:06d}'.format(i)
                )
                os.makedirs(testsavedir, exist_ok=True)
                print('test poses shape', noisy_extrinsic[i_val].shape)

                with torch.no_grad():


                    # transform_align: 4 X 4
                    # gt_test: N X 4 X 4
                    # Convert the focal to None (Not needed)

                    _hwf = hwf

                    if args.learn_expo:

                        evaluate_exposure(_hwf, mask if mask is not None else fisheye_mask,
                                          exposure_model, i_train, testsavedir)

                    if args.render_fisheye:
                        if mask is not None:
                            if len(mask.shape) >2:
                                render_mask = mask[i_val]
                            else:
                                render_mask = mask
                        else:
                            render_mask = fisheye_mask
                    else:
                        render_mask = None
                    rgbs, disps, psnrs = render_path(
                        gt_transformed_pose_val,
                        _hwf, args.chunk, render_kwargs_test,
                        gt_imgs=images[i_val],
                        savedir=testsavedir, mode="val",
                        camera_model=camera_model, args=args,
                        gt_intrinsic=raw_intrinsic,
                        gt_extrinsic=raw_extrinsic,
                        i_map=i_val,
                        transform_align=gt_transformed_pose_val,
                        mask=render_mask,
                    )

                writer.add_scalar('val/psnr',np.mean(psnrs))
                # writer.add_scalar('eval_img',)

        # Test Rendering
        if test_render:
            print("Starts validate Rendering")
            with torch.no_grad():
                testsavedir = os.path.join(
                    basedir, expname, 'testset_{:06d}'.format(i)
                )
                os.makedirs(testsavedir, exist_ok=True)
                print('test poses shape', noisy_extrinsic[i_val].shape)

                with torch.no_grad():

                    # transform_align: 4 X 4
                    # gt_test: N X 4 X 4
                    # Convert the focal to None (Not needed)


                    _hwf = hwf

                    # get test image set if available
                    try:
                        gt_test_imgs = images[i_test]
                    except:
                        gt_test_imgs = None

                    rgbs, disps, psnrs = render_path(
                        gt_transformed_pose_test,
                        _hwf, args.chunk, render_kwargs_test,
                        gt_imgs=gt_test_imgs,
                        savedir=testsavedir, mode="test",
                        camera_model=camera_model, args=args,
                        gt_intrinsic=raw_intrinsic,
                        gt_extrinsic=raw_extrinsic,
                        i_map=i_test,
                        transform_align=gt_transformed_pose_test,
                        mask=fisheye_mask if args.render_fisheye else None,
                    )

                if psnrs:
                    writer.add_scalar('test/mean_psnr',np.mean(psnrs))

        if (i % args.i_print == 0):

            print(
                "[TRAIN] Iter: {} Loss: {}  PSNR: {}".format(
                    i, train_loss.item(), train_psnr_1.item()
                )
            )

            writer.add_scalar('train/level_1_loss', train_loss.item(),i)
            writer.add_scalar('train/level_1_psnr',train_psnr_1.item(),i)

            if "rgb0" in extras:
                writer.add_scalar('train/level_0_loss', train_loss_0.item(), i)
                writer.add_scalar('train/level_0_psnr', train_psnr_0.item(), i)

        global_step += 1



if __name__ == '__main__':
    torch.set_default_tensor_type('torch.cuda.FloatTensor')
    train()
