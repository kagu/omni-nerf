# Omni-NeRF: NEURAL RADIANCE FIELD FROM 360° IMAGE CAPTURES
## [Project Page]() | [Paper](https://hal.inria.fr/hal-03646688/document) | [Video](https://drive.google.com/file/d/1yMRPW7LHUq9b8D4SGQxVYaFsrBTn5ZZk/view?usp=sharing) | [Data](https://drive.google.com/drive/folders/1qCG75zl0g-cH75_M3E2FAOvmRCqCNlDa?usp=sharing)

This repository contains the code release for the ICME 2022 paper [Omni-NeRF: Neural Radiance Field from 360° Image Captures](project link), 
the code is based on [SCNeRF](https://github.com/POSTECH-CVLab/SCNeRF) implementation. 

classroom | lone monk
--- | ---
![Teaser Image](classroom_learning.gif) | ![Teaser Image](lone_monk_learning.gif)

## Note
Please note the project with same name OmniNeRF, [Moving in a 360 World: Synthesizing Panoramic Parallaxes from a Single Panorama](https://cyhsu14.github.io/OmniNeRF/).
We are not intended to have the same name.

## Overview
This project tackles the problem of novel view synthesis (NVS)
from 360° images with imperfect camera poses or intrinsic
parameters. We propose a novel end-to-end framework for
training Neural Radiance Field (NeRF) models given only
360° RGB images, and their rough poses, which we refer to as
Omni-NeRF. We extend the pinhole camera model of NeRF to
a more general camera model that better fits omni-directional
fisheye lenses. The approach jointly learns the scene geometry and optimizes the camera parameters without knowing the
fisheye projection.

## Installation

We recommend to use [Anaconda](https://www.anaconda.com/products/individual) to set up the environment. First, create a new `omninerf` environment: 

You can create a new ```omni-nerf``` environment and install the dependencies by:

```conda env create -f environment.yml```

## Data

### Blender dataset

we render the virtual views of blender demo scenes with equisolid projection with blender-Cycles engine.
please find detailed information here [link](https://drive.google.com/drive/folders/1OMWUxwhWgmxx1fcbEAkEw8WxXvypkxpG?usp=sharing)

### Real dataset
- FTV Indoor sit [link](https://drive.google.com/file/d/1V-7XfW6B3Zdh_I-QzGHEjhWkniYFfzDO/view?usp=sharing)

**Note** the camera pose converted from FTV dataset can be problematic, though our method still works because of the optimizable camera poses. 
we are working on a quick solution.

the raw data was captured as synchronized [videos](https://project.inria.fr/ftv360/download/download-ftv-data/), we extracted the first frame of video sequences and separate the 
  front and rear views into 2 images. 

- Office [link](https://drive.google.com/file/d/1FcbaEkYZBI3vNQ9aLxObZYlz9fT262Ln/view?usp=sharing)

Put the data to ```data/``` or specify the directory in config files

### Running the code

#### training a new model

please replace {CONFIG} with the config you want to use. For example, you want to run an experiments with noisy pose 
initialization on classroom dataset, you would choose the config ```classroom_noisy_pose.txt```

```python run_omni_nerf.py --config configs/{CONFIG}```

#### Using a pre-trained model

To replicate results from the paper, you can find our pre-trained models [here](https://drive.google.com/drive/folders/1CiEJnwLlhbJSe_rQBB78v14-3nR_DfFZ?usp=sharing) and run the script with corresponding 
config. please put the folder to ```logs/``` 

## Citation
```
@INPROCEEDINGS{9859817,
  author={Gu, Kai and Maugey, Thomas and Knorr, Sebastian and Guillemot, Christine},
  booktitle={2022 IEEE International Conference on Multimedia and Expo (ICME)}, 
  title={Omni-NeRF: Neural Radiance Field from 360° Image Captures}, 
  year={2022},
  volume={},
  number={},
  pages={1-6},
  doi={10.1109/ICME52920.2022.9859817}}
```

