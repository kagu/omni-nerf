import configargparse

import sys
sys.path.insert(0, "..")

def config_parser():
    parser = configargparse.ArgumentParser()
    parser.add_argument('--config', is_config_file=True,
                        help='config file path')
    parser.add_argument("--expname", type=str,
                        help='experiment name')
    parser.add_argument("--basedir", type=str, default='./logs/',
                        help='where to store ckpts and logs')
    parser.add_argument("--datadir", type=str, default='./data/classroom',
                        help='input data directory')
    # training options
    parser.add_argument("--netdepth", type=int, default=8,
                        help='layers in network')
    parser.add_argument("--netwidth", type=int, default=256,
                        help='channels per layer')
    parser.add_argument("--netdepth_fine", type=int, default=8,
                        help='layers in fine network')
    parser.add_argument("--netwidth_fine", type=int, default=256,
                        help='channels per layer in fine network')
    parser.add_argument("--N_rand", type=int, default=32 * 32 * 4,
                        help='batch size (number of random rays per gradient step)')
    parser.add_argument("--lrate", type=float, default=5e-4,
                        help='learning rate')
    parser.add_argument("--lrate_decay", type=int, default=250,
                        help='exponential learning rate decay (in 1000 steps)')
    parser.add_argument("--chunk", type=int, default=1024 * 32,
                        help='number of rays processed in parallel, decrease if running out of memory')
    parser.add_argument("--netchunk_per_gpu", type=int, default=1024 * 64 * 4,
                        help='number of pts sent through network in parallel, decrease if running out of memory')
    parser.add_argument("--no_batching", action='store_true',
                        help='only take random rays from 1 image at a time')
    parser.add_argument("--no_reload", action='store_true',
                        help='do not reload weights from saved ckpt')
    parser.add_argument("--load_camera_only",action='store_true',
                        help='load optimized camera pose only for refining the nerf')
    parser.add_argument("--ft_path", type=str, default=None,
                        help='specific weights npy file to reload for coarse network')

    # learning rate and decay
    parser.add_argument('--nerf_lr', default=0.0005, type=float)
    parser.add_argument('--nerf_milestones', default=list(range(0, 200000, 10)), type=int, nargs='+',
                        help='learning rate schedule milestones')
    parser.add_argument('--nerf_lr_decay', type=float, default=400, help="learning rate milestones gamma")

    parser.add_argument('--camera_lr', default=0.001, type=float)
    parser.add_argument('--camera_milestones', default=list(range(0, 200000, 100)), type=int, nargs='+',
                        help='learning rate schedule milestones')
    parser.add_argument('--camera_lr_decay', type=float, default=150, help="learning rate milestones gamma")

    parser.add_argument('--exposure_lr', default=0.001, type=float)
    parser.add_argument('--exposure_milestones', default=list(range(0, 200000, 100)), type=int, nargs='+',
                        help='learning rate schedule milestones')
    parser.add_argument('--exposure_lr_decay', type=float, default=250, help="learning rate milestones gamma")


    # rendering options
    parser.add_argument("--N_samples", type=int, default=64,
                        help='number of coarse samples per ray')
    parser.add_argument("--N_iters", type=int, default=None,
                        help='number of iterations')
    parser.add_argument("--N_importance", type=int, default=0,
                        help='number of additional fine samples per ray')
    parser.add_argument("--perturb", type=float, default=1.,
                        help='set to 0. for no jitter, 1. for jitter')
    parser.add_argument("--use_viewdirs", action='store_true',
                        help='use full 5D input instead of 3D')
    parser.add_argument("--i_embed", type=int, default=0,
                        help='set 0 for default positional encoding, -1 for none')
    parser.add_argument("--multires", type=int, default=10,
                        help='log2 of max freq for positional encoding (3D location)')
    parser.add_argument("--multires_views", type=int, default=4,
                        help='log2 of max freq for positional encoding (2D direction)')
    parser.add_argument("--raw_noise_std", type=float, default=0.,
                        help='std dev of noise added to regularize sigma_a output, 1e0 recommended')

    parser.add_argument("--render_only", action='store_true',
                        help='do not optimize, reload weights and render out render_poses path')
    parser.add_argument("--render_test", action='store_true',
                        help='render the test set instead of render_poses path')
    parser.add_argument("--render_factor", type=int, default=0,
                        help='downsampling factor to speed up rendering, set 4 or 8 for fast preview')

    # training options
    parser.add_argument("--precrop_iters", type=int, default=0,
                        help='number of steps to train on central crops')
    parser.add_argument("--precrop_frac", type=float,
                        default=.5, help='fraction of img taken for central crops')

    # fisheye dataset and scene options
    parser.add_argument("--dataset_type", type=str, default='llff',
                        help='options: llff / blender / deepvoxels')
    parser.add_argument("--testskip", type=int, default=8,
                        help='will load 1/N images from test/val sets, useful for large datasets like deepvoxels')
    parser.add_argument("--lindisp", action='store_true',
                        help='sampling linearly in disparity rather than depth')
    parser.add_argument(
        "--train_fisheye", action='store_true',
    )
    parser.add_argument(
        "--render_fisheye", action='store_true',
    )

    parser.add_argument("--radius",type=float, default=-1, help='radius for the fisheye mask, in pixels')
    parser.add_argument("--near",type=float, default=0.0, help='near bound of sampling sphere')
    parser.add_argument("--far",type=float,default = 8.0, help='far bound of sampling sphere')
    parser.add_argument("--coeff_init", nargs=3, type=float, default=[0,0,0],
                        help="initialization of radial distortion coefficients")
    parser.add_argument("--factor", type=float, default=1)

    ## blender flags
    parser.add_argument("--white_bkgd", action='store_true',
                        help='set to render synthetic data on a white bkgd (always use for dvoxels)')
    parser.add_argument("--half_res", action='store_true',
                        help='load blender synthetic data at 400x400 instead of 800x800')

    # logging/saving options
    parser.add_argument("--i_print", type=int, default=100,
                        help='frequency of console printout and metric loggin')
    parser.add_argument("--i_img", type=int, default=500,
                        help='frequency of tensorboard image logging')
    parser.add_argument("--i_weights", type=int, default=10000,
                        help='frequency of weight ckpt saving')
    parser.add_argument("--i_testset", type=int, default=50000,
                        help='frequency of testset saving')
    parser.add_argument("--i_video", type=int, default=50000,
                        help='frequency of render_poses video saving')


    # Injecting noises in camera parameters
    parser.add_argument(
        "--initial_noise_size_intrinsic",
        type=float,
        default=0.00,
        help="initial noise size in intrinsic parameters"
    )

    parser.add_argument(
        "--initial_noise_size_translation",
        type = float,
        default = 0.00,
        help = "initial noise size in translation paraemters",
    )

    parser.add_argument(
        "--initial_noise_size_rotation",
        type = float,
        default = 0.00,
        help = "initial noise size in rotation matrices",
    )

    parser.add_argument(
        "--delta_rotation_scale",
        type=float,
        default=1,
        help="rotation noise scale when the learning is noise learning. ",
    )
    parser.add_argument(
        "--delta_translation_scale",
        type=float,
        default=1,
        help="translation noise scale when the learning is noise learning.",
    )


    parser.add_argument(
        "--seed",
        type=int,
        default=777,
        help="seed to fix"
    )

    # Curriculum Learning

    parser.add_argument(
        "--add_i", default=1000, type=int,
        help="step to start learning i"
    )
    parser.add_argument(
        "--add_e", default=1000, type=int,
        help="step to start learning e"
    )
    parser.add_argument(
        "--add_radial", default=500, type=int,
        help="step to start learning distortion"
    )

    parser.add_argument('--focal_len', type=float, default=208.33333)

    parser.add_argument('--render_focal', type=float, default=13.0/36*600)

    parser.add_argument('--learn_e',action='store_true')

    parser.add_argument('--learn_i',action='store_true')

    parser.add_argument('--learn_radial',action='store_true')

    parser.add_argument('--learn_expo',action='store_true')

    parser.add_argument('--use_GEAR360', action='store_true',
                        help='use relative pose for gear 360 front and rear lenses')



    parser.add_argument(
        "--use_custom_optim",
        type=bool,
        nargs="?",
        const=True,
        default=False,
        help= "Adopt custom optimizer"
    )


    return parser


