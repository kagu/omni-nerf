import torch
import torch.nn as nn
import numpy as np


class ExposureModel(nn.Module):

    # learn auto-exposure for cameras individually, experimental feature from nerf++
    # learn the vignette correction for all the camera, especially for fisheye camera
    # regularize may be necessary for better performance

    def __init__(self, num_cams, H, W, learn_expo=False, learn_vignette=False):
        super(ExposureModel, self).__init__()

        self.x_c = W / 2.0 - 0.5
        self.y_c = H / 2.0 - 0.5
        self.r = np.sqrt(W ** 2 + H ** 2) / 2

        self.expo = nn.Parameter(torch.stack([torch.tensor([0.5, 0], dtype=torch.float32) for i in range(num_cams)],
                                             dim=0), requires_grad=learn_expo)
        self.vignette = nn.Parameter(torch.tensor([0.0, 0.0, 0.0], dtype=torch.float32), requires_grad=learn_vignette)

    def forward(self, cam_id, u, v):

        u = torch.tensor(u,dtype=torch.float32, device=self.expo.device)
        v = torch.tensor(v, dtype=torch.float32, device=self.expo.device)

        # a radius dependent vignette term
        u = u - self.x_c
        v = v - self.y_c
        a, b, c = self.vignette
        r = torch.sqrt((u) ** 2 + (v) ** 2) / self.r
        vig = a *(r + b * r ** 2 + c * r ** 3)

        expo = self.expo[cam_id]
        # return the scale term and the shift term
        return expo[:,0], expo[:,1]+vig